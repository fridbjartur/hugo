# User system app – Documentation

## Table of contents

[Dockerfile](#01)  
[dockerfile-compose.yml](#02)  
[PHP unit testing](#03)  
[Gitlab pipeline](#04)

## Dockerfile {#01}

You are already using a virtual machine, whether you know it or not.
Your `kea-dev` server is in fact a virtual machine.

    FROM php:7.3-apache
    RUN docker-php-ext-install mysqli

## dockerfile-compose.yml {#02}

You are already using a virtual machine, whether you know it or not.
Your `kea-dev` server is in fact a virtual machine.

    version: '3.1'
    services:
    www:
        build: .
        ports:
        - '8091:80'
        volumes:
        - ./www:/var/www/html/
        links:
        - db
        networks:
        - default
    db:
        image: mysql:8.0
        ports:
        - '3308:3308'
        command: --default-authentication-plugin=mysql_native_password
        environment:
        MYSQL_DATABASE: dockerExample
        MYSQL_USER: user
        MYSQL_PASSWORD: test
        MYSQL_ROOT_PASSWORD: test
        volumes:
        - ./dump:/docker-entrypoint-initdb.d
        - ./conf:/etc/mysql/conf.d
        - persistent:/var/lib/mysql
        networks:
        - default
    phpmyadmin:
        image: phpmyadmin/phpmyadmin
        links:
        - db:db
        ports:
        - 8092:80
        environment:
        MYSQL_USER: user
        MYSQL_PASSWORD: test
        MYSQL_ROOT_PASSWORD: test
    volumes:
    persistent:
    networks:
    frontend:

## PHP unit testing {#03}

You are already using a virtual machine, whether you know it or not.
Your `kea-dev` server is in fact a virtual machine.

## Gitlab pipeline {#04}

You are already using a virtual machine, whether you know it or not.
Your `kea-dev` server is in fact a virtual machine.
